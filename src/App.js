import React, { useContext, useReducer } from 'react'
import { UserContext } from './index'


const INITIAL_STATE = {
  count: 0
}

function reducer(state, action) {
  switch (action.type) {

    case "increment":
      return {
        count: state.count + 1
      }
    case "decrement":
      return {
        count: state.count - 1
      }
    case "reset":
      return INITIAL_STATE

    default:
      return INITIAL_STATE
  }
}

export default function App() {

  const [state, dispatch] = useReducer(reducer, INITIAL_STATE)
  const value = useContext(UserContext)

  return (
    <div>
      Count: {state.count}
      <button
        className="border m-1 p-1"
        onClick={() => dispatch({ type: "increment" })}>
        {value} Add
      </button>
      <button
        className="border m-1 p-1"
        onClick={() => dispatch({ type: "decrement" })}>
        {value} Minus
      </button>
      <button
        className="border m-1 p-1"
        onClick={() => dispatch({ type: "reset" })}>
        {value} Reset
      </button>
    </div>
  )
}

