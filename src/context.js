import React, {createContext} from 'react'

const TodosContext = createContext({
    todos: [
        // {id: 1, text: "Create", complete:false},
        // {id: 2, text: "Read", complete:false},
        // {id: 3, text: "Write", complete:false},
        // {id: 4, text: "Delete", complete:true}
    ],
    currentTodo: {}
})

export default TodosContext
